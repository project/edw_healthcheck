<?php

namespace Drupal\edw_healthcheck\Controller;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\edw_healthcheck\Plugin\EDWHealthCheckPlugin\EDWHealthCheckPluginManager;
use Drupal\edw_healthcheck\Render\JsonEDWHealthCheckRender;
use Drupal\update\UpdateManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class EDWHealthCheckPageController.
 *
 * Produces the HTTP output that the bash script understands.
 *
 * @package Drupal\edw_healthcheck\Controller
 */
class EDWHealthCheckPageController extends ControllerBase {

  /**
   * The EDWHealthCheck plugin manager.
   *
   * We use this to get all of the EDWHealthCheck plugins.
   *
   * @var EDWHealthCheckPluginManager
   */
  protected $pluginManager;

  /** @var UpdateManager */
  protected $updateManager;

  protected $definitions = [];

  /**
   * The EDWHealthCheck module's config.
   *
   * We use this to get all of the EDWHealthCheck configuration settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.edw_healthcheck'),
      $container->get('update.manager'),
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  public function __construct(
    EDWHealthCheckPluginManager $pluginManager,
    UpdateManager $updateManager,
    ConfigFactory $configFactory,
    ModuleHandler $moduleHandler
  ) {
    $this->pluginManager = $pluginManager;
    $this->updateManager = $updateManager;
    $this->config = $configFactory->get('edw_healthcheck.settings');
    $this->definitions = $pluginManager->getDefinitions();
    $this->moduleHandler = $moduleHandler;
    $this->database = \Drupal::database();
  }

  /**
   * Main function building the string to show via HTTP.
   *
   * @param string $type
   *   Optional parameter that can specify the information needed for rendering.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns an Http Response containing the processed output by the module.
   *
   * @throws PluginException
   *    The Plugin Exception needs to be handled.
   */
  public function content($type = NULL) {
    if (!$this->config->get('edw_healthcheck.statuspage.enabled')) {
      throw new AccessDeniedHttpException("Edw health check page is disabled!");
    }

    $response = (new Response(NULL, Response::HTTP_OK, ['Content-Type' => 'text/json']))
      ->setMaxAge(0)
      ->setExpires(NULL);

    if (in_array($type, ['all', 'core', 'modules'])) {
      $this->updateManager->refreshUpdateData();
    }
    $data = [];
    if ($type == 'all') {
      foreach ($this->definitions as $definitionId => $definition) {
        $data = array_merge($data, $this->getPluginData($definitionId));
      }
    }
    else {
      $data = $this->getPluginData($this->getDefinitionIdForType($type));
    }

    $render_instance = new JsonEDWHealthCheckRender();
    $content = $render_instance->render($data);

    $response->setContent($content);

    return $response;
  }

  /**
   * Gets a repetitive warnings report if dblog module is active.
   *
   * @return array
   *    The render array containing a table of repetitive warnings.
   */
  public function repetitiveWarnings() {
    if (!$this->moduleHandler->moduleExists('dblog')) {
      throw new NotFoundHttpException("This website doesn't have dblog module installed.");
    }

    $repetitiveWarnings = $this->database
      ->query("
SELECT CONCAT(type, ' - ', message) AS message, COUNT(message) AS count, severity
FROM {watchdog}
where severity <= 4
GROUP BY type, message, severity
HAVING COUNT(message) >= 5
ORDER BY COUNT(message) DESC");

    $severityMap = RfcLogLevel::getLevels();

    $header = ['Severity', 'Message', 'Count'];
    $rows = [];
    if ($repetitiveWarnings) {
      while ($result = $repetitiveWarnings->fetchAssoc()) {
        $rows[] = [
          $severityMap[$result['severity']],
          $result['message'],
          $result['count'],
        ];
      }
    }
    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
  }

  /**
   * @param $type
   *
   * @return int|string|null
   */
  protected function getDefinitionIdForType($type) {
    foreach ($this->definitions as $definitionId => $definition) {
      if (empty($definition['type'])) {
        continue;
      }

      if ($type != $definition['type']) {
        continue;
      }

      return $definitionId;
    }

    return NULL;
  }

  /**
   * @param string $definitionId
   *    The type of the component that needs to be handled.
   *
   * @return array
   *    The data array processed by the plugin.
   *
   * @throws PluginException
   *    The createInstance method of the Plugin Manager can throw a Plugin
   *    Exception.
   */
  protected function getPluginData($definitionId) {
    $data = [];

    if (empty($this->definitions[$definitionId])) {
      return $data;
    }

    $definition = $this->definitions[$definitionId];
    if (empty($definition['type'])) {
      return $data;
    }

    if ($this->config->get('edw_healthcheck.components.' . $definition['type'] . '.enabled')) {
      $plugin = $this->pluginManager->createInstance($definitionId);
      $data = $plugin->getData();
    }

    return $data;
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function viewMaintenanceStatus() {
    $maintenanceMode = $this->state()->get('system.maintenance_mode');
    $message = $maintenanceMode ? 'The site is currently in maintenance mode.' : 'The site is currently live.';

    return (new Response($message, Response::HTTP_OK, ['Cache-Control' => 'no-cache, must-revalidate']))
      ->setMaxAge(0)
      ->setExpires();
  }

}
