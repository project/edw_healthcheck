<?php

namespace Drupal\edw_healthcheck\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\edw_healthcheck\Plugin\EDWHealthCheckPlugin\EDWHealthCheckPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows admins to configure the EDWHealthCheck module.
 *
 * @package Drupal\edw_healthcheck\Form
 */
class SettingsForm extends ConfigFormBase {

  /** @var \Drupal\edw_healthcheck\Plugin\EDWHealthCheckPlugin\EDWHealthCheckPluginManager */
  protected $pluginManager;


  public function __construct(ConfigFactoryInterface $config_factory, EDWHealthCheckPluginManager $pluginManager, TypedConfigManagerInterface $typedConfigManager) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->pluginManager = $pluginManager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.edw_healthcheck'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'edw_healthcheck_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'edw_healthcheck.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('edw_healthcheck.settings');

    $form['edw_healthcheck_status_page'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => $this->t('Status page settings'),
    ];
    $form['edw_healthcheck_status_page']['edw_healthcheck_enable_status_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable the EDWHealthCheck status page.'),
      '#default_value' => $config->get('edw_healthcheck.statuspage.enabled'),
    ];

    $form['edw_healthcheck_components'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => $this->t('EDWHealthCheck components settings'),
    ];

    foreach ($this->pluginManager->getDefinitions() as $id => $definition) {
      if (empty($definition['type'])) {
        continue;
      }

      $type = $definition['type'];

      $form['edw_healthcheck_components']["edw_healthcheck_enable_{$type}_component"] = [
        '#type' => 'checkbox',
        '#title' => $definition['description'],
        '#default_value' => $config->get("edw_healthcheck.components.{$type}.enabled"),
        '#attributes' => ($config->get('edw_healthcheck.statuspage.enabled') == FALSE ? ['disabled' => TRUE] : []),
        '#states' => [
          'disabled' => [
            ':input[name="edw_healthcheck_enable_status_page"]' => ['checked' => FALSE],
          ],
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('edw_healthcheck.settings');
    $status_page = $form_state->getValue('edw_healthcheck_enable_status_page');
    $config->set('edw_healthcheck.statuspage.enabled', (bool) $status_page);

    foreach ($this->pluginManager->getDefinitions() as $id => $definition) {
      if (empty($definition['type'])) {
        continue;
      }

      $type = $definition['type'];
      $config->set("edw_healthcheck.components.{$type}.enabled", $form_state->getValue("edw_healthcheck_enable_{$type}_component"));

    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
