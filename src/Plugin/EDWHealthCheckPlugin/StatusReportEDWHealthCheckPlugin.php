<?php

namespace Drupal\edw_healthcheck\Plugin\EDWHealthCheckPlugin;

use Drupal\Core\Database\Database;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element\StatusReport;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Provides a EDWHealthCheck plugin that manages Warnings information.
 *
 * @EDWHealthCheckPlugin(
 *   id = "status_report_edw_healthcheck",
 *   description = @Translation("Status report of the project."),
 *   type = "status_report"
 * )
 */
class StatusReportEDWHealthCheckPlugin extends EDWHealthCheckPluginBase implements ContainerFactoryPluginInterface, EDWHealthCheckPluginInterface {

  /** @var string */
  protected $absoluteBaseUrl;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, TranslationInterface $translation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $translation);
    $this->absoluteBaseUrl = \Drupal::request()->getSchemeAndHttpHost() . '/';
  }

  /**
   * @return array
   *   An array that contains the information relevant to the plugin's type.
   */
  public function getData() {
    $requirements = \Drupal::service('system.manager')->listRequirements();
    $element = [
      '#requirements' => $requirements,
      '#priorities' => [
        'error',
        'warning',
        'checked',
        'ok',
      ],
    ];
    $requirements = StatusReport::preRenderGroupRequirements($element);
    $errors = $this->getProcessedMessages($requirements['#grouped_requirements']['error']['items']);
    $warnings = $this->getProcessedMessages($requirements['#grouped_requirements']['warning']['items']);
    $readCommitted = $this->getReadCommitted();

    return [
      'status_report' => [
        'project_type' => 'status_report',
        'database_read_committed' => $readCommitted,
        'errors_found' => $errors,
        'warnings_found' => $warnings,
        'additional_warnings_found' => $this->getAdditionalWarnings(),
      ],
    ];
  }

  protected function getReadCommitted() {
    $databaseInfo = Database::getConnectionInfo('default');
    $initCommands = $databaseInfo['default']['init_commands'];
    return (!empty($initCommands) &&
      array_key_exists('isolation', $initCommands) &&
      strpos(strtoupper($initCommands['isolation']),
        'READ-COMMITTED') !== FALSE);
  }

  protected function getProcessedMessages($items): array {
    $messages = [];

    foreach ($items as $key => $item) {
      $title = $item['title'] ?? '';
      if (!empty($title)) {
        if (is_array($title)) {
          $title = \Drupal::service('renderer')->render($title);
        }
        $title = html_entity_decode($title, ENT_QUOTES, 'UTF-8');
      }

      $value = $item['value'] ?? '';
      if (!empty($value)) {
        if (is_array($value)) {
          $value = \Drupal::service('renderer')->render($value);
        }
        $value = html_entity_decode($value, ENT_QUOTES, 'UTF-8');
        $value = str_replace("href=\"/", "href=\"{$this->absoluteBaseUrl}", $value);
      }

      $description = $item['description'] ?? '';
      if (!empty($description)) {
        if (is_array($description)) {
          $description = \Drupal::service('renderer')->render($description);
        }
        $description = html_entity_decode($description, ENT_QUOTES, 'UTF-8');
        $description = str_replace("href=\"/", "href=\"{$this->absoluteBaseUrl}", $description);
      }

      $messages[$key] = [
        'title' => $title,
        'value' => $value,
        'description' => $description,
      ];
    }

    return $messages;
  }

  public function getAdditionalWarnings(): array {
    $warnings = [];
    $moduleHandler = \Drupal::getContainer()->get('module_handler');
    if ($moduleHandler->load('dblog')) {
      $logKeep = \Drupal::configFactory()
        ->getEditable('dblog.settings')
        ->get('row_limit');
      if ($logKeep == 0) {
        $warnings['dblog'] = [
          'title' => 'Log messages limit',
          'value' => $this->t('There is not limit on how many messages are kept in the database log.'),
          'description' => NULL,
        ];
      }
      if ($logKeep > 100000) {
        $warnings['dblog'] = [
          'title' => 'Log messages limit',
          'value' => $this->t('The maximum number of messages to keep in the database log is @number.', [
            '@number' => $logKeep,
          ]),
          'description' => NULL,
        ];
      }
    }

    return $warnings;
  }

}
