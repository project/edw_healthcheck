<?php

namespace Drupal\edw_healthcheck\Plugin\EDWHealthCheckPlugin;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a EDWHealthCheck plugin that manages Theme information.
 *
 * The following is the plugin annotation. This is parsed by Doctrine to make
 * the plugin definition. Any values defined here will be available in the
 * plugin definition.
 *
 * This should be used for metadata that is specifically required to instantiate
 * the plugin, or for example data that might be needed to display a list of all
 * available plugins where the user selects one. This means many plugin
 * annotations can be reduced to a plugin ID, a label and perhaps a description.
 *
 * @EDWHealthCheckPlugin(
 *   id = "themes_edw_healthcheck",
 *   description = @Translation("Information about the Themes of the project."),
 *   type = "themes"
 * )
 */
class ThemesEDWHealthCheckPlugin extends EDWHealthCheckPluginBase implements ContainerFactoryPluginInterface, EDWHealthCheckPluginInterface {

  /**
   * Retrieve the data relevant to the plugin's type.
   *
   * @return array
   *   An array that contains the information relevant to the plugin's type.
   */
  public function getData() {
    $themes = \Drupal::service('theme_handler')->rebuildThemeData();

    $data = [];
    foreach ($themes as $theme_name => $desc) {
      $data[$theme_name] = (array) $desc;
      $data[$theme_name]['project_type'] = 'theme';
    }
    return $data;
  }

}
