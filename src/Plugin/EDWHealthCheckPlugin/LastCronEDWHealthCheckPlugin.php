<?php

namespace Drupal\edw_healthcheck\Plugin\EDWHealthCheckPlugin;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a EDWHealthCheck plugin that manages Cron information.
 *
 * This plugin stores information on the last cron execution.
 *
 * @EDWHealthCheckPlugin(
 *   id = "last_cron_edw_healthcheck",
 *   description = @Translation("Information about the last run cron of the project."),
 *   type = "last_cron"
 * )
 */
class LastCronEDWHealthCheckPlugin extends EDWHealthCheckPluginBase implements ContainerFactoryPluginInterface, EDWHealthCheckPluginInterface {

  use StringTranslationTrait;

  /**
   * The request time.
   *
   * @var int
   */
  protected $requestTime;

  /**
   * The last cron run.
   *
   * @var int
   */
  protected $lastCronRun;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('state')->get('system.cron_last'),
      $container->get('datetime.time')->getRequestTime()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TranslationInterface $translation, $last_cron_run, $request_time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $translation);
    $this->lastCronRun = $last_cron_run;
    $this->requestTime = $request_time;
  }

  /**
   * Retrieve the data relevant to the plugin's type.
   *
   * @return array
   *   An array that contains the information relevant to the plugin's type.
   */
  public function getData() {
    $lastRun = (gmdate("H", $this->lastCronRun) + 3) . ":" . gmdate("i:s", $this->lastCronRun);
    return [
      'last_cron_plugin' => [
        'last_cron_run' => $lastRun,
        'timestamp' => $this->lastCronRun,
        'request_time' => $this->requestTime,
        'active_and_running' => $this->checkCronStatus(),
        'project_type' => 'last_cron',
      ],
    ];
  }

  /**
   * Get the status of the cron, compared with the last run.
   *
   * @return bool
   *   Returns false if the cron has not ran in 6 hours.
   */
  public function checkCronStatus() {
    return $this->lastCronRun + $this->getFailureThreshold() > $this->requestTime;
  }

  /**
   * Get the failure threshold configured for this plugin.
   *
   * @return int
   *   The failure threshold in seconds. Defaults to 6 hours.
   */
  private function getFailureThreshold() {
    return 21600;
  }

}
