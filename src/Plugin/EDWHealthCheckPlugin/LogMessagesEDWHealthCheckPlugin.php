<?php

namespace Drupal\edw_healthcheck\Plugin\EDWHealthCheckPlugin;


use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Provides a EDWHealthCheck plugin that gives information about log messages.
 *
 * @EDWHealthCheckPlugin(
 *   id = "log_messages_report_edw_healthcheck",
 *   description = @Translation("Log messages of the project."),
 *   type = "log_report"
 * )
 */
class LogMessagesEDWHealthCheckPlugin extends EDWHealthCheckPluginBase implements ContainerFactoryPluginInterface, EDWHealthCheckPluginInterface {

  /**
   * Database conection.
   *
   * @var \Drupal\Core\Database\Connection
   *
   */
  protected $database;

  /**
   * Watchdog severity codes.
   *
   * @var array
   */
  const severityCodes = [
    'error' => 3,
    'notice' => 5,
    'warning' => 4,
    'emergency' => 0,
    'alert' => 1,
    'critical' => 2,
  ];

  public function __construct(array $configuration, $plugin_id, $plugin_definition, TranslationInterface $translation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $translation);
    $this->database = \Drupal::database();
  }

  public function getData() {
    $moduleHandler = \Drupal::service('module_handler');
    if (!$moduleHandler->moduleExists('dblog')) {
      return [];
    }

    $repetitiveWarnings = $this->database
      ->query("SELECT CONCAT(type, ' - ', message), COUNT(message) FROM {watchdog} where severity <= 4 GROUP BY type,message HAVING COUNT(message) >= 5 ORDER BY COUNT(message) DESC");
    $noRepetitive = count($repetitiveWarnings->fetchAll());

    $noErrors = $this->database
      ->select('watchdog')
      ->condition('severity', self::severityCodes['error'])
      ->countQuery()
      ->execute()
      ->fetchField();

    $noNotice = $this->database
      ->select('watchdog')
      ->condition('severity', self::severityCodes['notice'])
      ->countQuery()
      ->execute()
      ->fetchField();

    $noWarnings = $this->database
      ->select('watchdog')
      ->condition('severity', self::severityCodes['warning'])
      ->countQuery()
      ->execute()
      ->fetchField();

    $noAlerts = $this->database
      ->select('watchdog')
      ->condition('severity', self::severityCodes['alert'])
      ->countQuery()
      ->execute()
      ->fetchField();

    $noCritical = $this->database
      ->select('watchdog')
      ->condition('severity', self::severityCodes['critical'])
      ->countQuery()
      ->execute()
      ->fetchField();

    $noEmergencies = $this->database
      ->select('watchdog')
      ->condition('severity', self::severityCodes['emergency'])
      ->countQuery()
      ->execute()
      ->fetchField();

    return [
      'recent_log_messages_report' => [
        'project_type' => 'log_messages',
        'error' => $noErrors,
        'notice' => $noNotice,
        'warning' => $noWarnings,
        'critical' => $noCritical,
        'alert' => $noAlerts,
        'emergency' => $noEmergencies,
        'repetitive_warnings' => $noRepetitive,
      ],
    ];
  }
}
