<?php

namespace Drupal\edw_healthcheck\Plugin\EDWHealthCheckPlugin;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a EDWHealthCheck plugin that manages Module information.
 *
 * The following is the plugin annotation. This is parsed by Doctrine to make
 * the plugin definition. Any values defined here will be available in the
 * plugin definition.
 *
 * This should be used for metadata that is specifically required to
 * instantiate
 * the plugin, or for example data that might be needed to display a list of
 * all
 * available plugins where the user selects one. This means many plugin
 * annotations can be reduced to a plugin ID, a label and perhaps a
 * description.
 *
 * @EDWHealthCheckPlugin(
 *   id = "modules_edw_healthcheck",
 *   description = @Translation("Information about the Modules of the project."),
 *   type = "modules"
 * )
 */
class ModulesEDWHealthCheckPlugin extends EDWHealthCheckPluginBase implements ContainerFactoryPluginInterface, EDWHealthCheckPluginInterface {

  /**
   * Retrieve the data relevant to the plugin's type.
   *
   * @return array
   *   An array that contains the information relevant to the plugin's type.
   */
  public function getData() {
    $data = [];
    $available = update_get_available(TRUE);
    if (empty($available)) {
      return $data;
    }

    \Drupal::moduleHandler()->loadInclude('update', 'inc', 'update.compare');
    $moduleList = update_calculate_project_data($available);
    foreach ($moduleList as $name => $info) {
      if ($name == "drupal") {
        continue;
      }
      $data[$name] = $info;
    }

    return $data;
  }

}
