<?php

namespace Drupal\edw_healthcheck\Plugin\EDWHealthCheckPlugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class to help developers implement their own edw_healthcheck plugins.
 *
 * In this case all the properties can be read from the @EDWHealthCheckPlugin
 * annotation.
 *
 * In most cases it is probably fine to just use that value without any
 * additional processing. However, if an individual plugin needed to provide
 * special handling around either of these things it could just override
 * the method in that class definition for that plugin.
 *
 * @EDWHealthCheckPlugin(
 *   id = "abstract_edw_healthcheck",
 *   description = @Translation("Base edw healthcheck"),
 *   type = "abstract"
 * )
 */
abstract class EDWHealthCheckPluginBase extends PluginBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TranslationInterface $translation) {
    // Store the translation service.
    $this->setStringTranslation($translation);
    // Pass the other parameters up to the parent constructor.
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    // Retrieve the @description property from the annotation and return it.
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function type() {
    // Retrieve the @type property from the annotation and return it.
    return $this->pluginDefinition['type'];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getData();

  /**
   * Generate the form information specific to the plugin.
   *
   * @return array
   *   An array built with the settings form information for the plugin.
   */
  public function form() {
    // To be implemented in a later release.
    return [];
  }

}
