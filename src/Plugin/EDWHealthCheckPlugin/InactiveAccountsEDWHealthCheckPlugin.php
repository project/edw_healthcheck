<?php

namespace Drupal\edw_healthcheck\Plugin\EDWHealthCheckPlugin;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a EDWHealthCheck plugin that manages inactive accounts information.
 *
 * This plugin stores information on the last cron execution.
 *
 * @EDWHealthCheckPlugin(
 *   id = "inactive_accounts_edw_healthcheck",
 *   description = @Translation("Inactive accounts"),
 *   type = "inactive_accounts"
 * )
 */
class InactiveAccountsEDWHealthCheckPlugin extends EDWHealthCheckPluginBase implements ContainerFactoryPluginInterface, EDWHealthCheckPluginInterface {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TranslationInterface $translation, Request $request, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $translation);
    $this->request = $request;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Retrieve the data relevant to the plugin's type.
   *
   * @return array
   *   An array that contains the information relevant to the plugin's type.
   */
  public function getData() {
    $userStorage = $this->entityTypeManager->getStorage('user');
    $timestamp = $this->request->query->get('inactive_accounts');
    $query = $userStorage->getQuery()->accessCheck(FALSE);
    $query->condition('login', $timestamp, '<');
    $query->condition('status', 1);
    $query->condition('uid', 0, '>');
    $users = $query->execute();
    /** @var \Drupal\user\Entity\User[] $users */
    $users = $userStorage->loadMultiple($users);
    $names = [];
    foreach ($users as $user) {
      $names[$user->id()] = $user->getAccountName();
    }
    if (empty($timestamp)) {
      return [];
    }

    return [
      'inactive_accounts' => [
        'accounts' => $names,
        'timestamp' => $this->request->query->get('inactive_accounts'),
        'project_type' => 'inactive_accounts',
      ],
    ];
  }

}
